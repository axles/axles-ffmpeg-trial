package andorid.slowmodemo.com.extentions

import android.content.Context
import java.io.File

fun getInternalStorage(context: Context): File {
    val file = File(context.filesDir.absolutePath)

    return file
}

fun createStorageDirFile(context: Context, name: String): File {
    val storageDir = getInternalStorage(context)
    return File(storageDir.path + File.separator + name)
}