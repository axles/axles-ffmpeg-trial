package andorid.slowmodemo.com.model

data class TimeLine(val feature: Feature, val startTime: Long, val endTime: Long)

enum class Feature {
    NORMAL,
    SLOW_MO,
    SPEEDUP
}