package andorid.slowmodemo.com.view

import andorid.slowmodemo.com.R
import andorid.slowmodemo.com.extentions.createStorageDirFile
import andorid.slowmodemo.com.model.Feature
import andorid.slowmodemo.com.model.TimeLine
import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.arthenica.mobileffmpeg.*
import com.otaliastudios.cameraview.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File


class MainActivity : AppCompatActivity() {
    private lateinit var originalVideoFile: File
    private lateinit var finalFile: File

    private var keyTime = 0L
    private var currentFeature: Feature = Feature.NORMAL
    private var startVideoTime = 0L
    private var endVideoTime = 0L
    private val timeLine = mutableListOf<TimeLine>()
    private val disposable = CompositeDisposable()

    private val addToList = {
            t: TimeLine, index: Int, aNormalOptions: String, vNormalOptions: String  ->
        val start = format(t.startTime)
        val end = format(t.endTime)
        addOption(start, end, index, aNormalOptions, vNormalOptions)
    }
    private val atempo = {value: Float ->
        "atempo=$value,atempo=$value"
    }
    private val setPtsSpeed = {value: Float ->
        "setpts=$value*PTS"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setLayoutNoLimits()
        setHardwareAcceleration()
        setContentView(R.layout.activity_main)
        createVideoFiles()

        if (checkCameraPermissions()) {
            setCameraVisibility(View.GONE)
            requestCameraPermissions()
        } else {
            initCamera()
        }

        hideToolbar()
        setupClickListeners()
        enableConfigs()
    }

    override fun onResume() {
        super.onResume()
        camera_view.open()
    }

    override fun onPause() {
        super.onPause()
        camera_view.close()
    }

    override fun onDestroy() {
        camera_view.destroy()
        FFmpeg.cancel()
        disposable.clear()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ACTIVITY_FOR_RESULTS_CODE && resultCode == Activity.RESULT_OK) {
            when(resultCode) {
                Activity.RESULT_OK -> processVideo()
                else -> clearTimeLine()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_CODE -> onCameraPermissionResult(grantResults)
        }
    }

    private fun setupClickListeners() {
        record_btn.setOnCheckedChangeListener(getOnRecordCheckedChangeListener())

        features_rg.setOnCheckedChangeListener(getFeaturesCheckedListener())
    }

    private fun getFeaturesCheckedListener()
            = RadioGroup.OnCheckedChangeListener { _, checkedId ->
        val isRecordChecked = record_btn.isChecked
        when (checkedId) {
            slowmo.id -> setMode(Feature.SLOW_MO, isRecordChecked)
            normal.id -> setMode(Feature.NORMAL, isRecordChecked)
            speedup.id -> setMode(Feature.SPEEDUP, isRecordChecked)
        }
    }

    private fun getOnRecordCheckedChangeListener() =
        CompoundButton.OnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                deleteFile(originalVideoFile)
                camera_view.takeVideo(originalVideoFile)
                setCountDownText("3")
                getCountDown().start()
            } else {
                camera_view.stopVideo()
                endVideoTime = currentTime()
                addToTimeLine(currentFeature, keyTime, currentTime(), timeLine, !record_btn.isChecked )
                setRecordBtnTint(R.color.colorWhite)
            }
    }

    private fun getCountDown() = object : CountDownTimer(1500, 1) {
        override fun onFinish() {
            setCountDownText("")
            showProgress(false)
            startVideoTime = currentTime()
            keyTime = currentTime()
            setRecordBtnTint(R.color.colorRed)
        }

        override fun onTick(millisUntilFinished: Long) {
            when(millisUntilFinished) {
                in 900..1100L -> setCountDownText("2")
                in 400L..500L -> setCountDownText("1")
            }
        }
    }

    private fun setRecordBtnTint(@ColorRes colorId: Int) {
        record_btn.backgroundTintList = getColorState(colorId)
    }

    private fun getColorState(@ColorRes colorId: Int)
            = ContextCompat.getColorStateList(this@MainActivity, colorId)

    private fun hideToolbar() = supportActionBar?.hide()

    private fun requestCameraPermissions() = ActivityCompat.requestPermissions(
        this,
        arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO),
        REQUEST_CODE)

    private fun checkCameraPermissions() = (
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
                    != PackageManager.PERMISSION_GRANTED)

    private fun setWindowFrags(flag: Int, mask: Int) = window.setFlags(flag, mask)

    private fun setLayoutNoLimits() =
        setWindowFrags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)

    private fun setHardwareAcceleration() =
        setWindowFrags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED)

    private fun createVideoFiles() {
        originalVideoFile = createStorageDirFile(this, ORIGINAL_FILE_NAME)
        finalFile = createStorageDirFile(this, FINAL_FILE_NAME)
    }

    private fun enableConfigs() {
        Config.enableLogCallback {
                message -> Log.e(Config.TAG, message.text)
        }
        Config.enableStatisticsCallback { newStatistics ->
            Log.e(
                Config.TAG,
                "frame: ${newStatistics.videoFrameNumber}, time: ${newStatistics.time}"
            )
        }
        Config.setLogLevel(Level.AV_LOG_INFO)
    }

    private fun setCountDownText(text: String) {
        count_down.text = text
    }

    private fun setMode(feature: Feature, isRecordChecked: Boolean) {
        addToTimeLine(currentFeature, keyTime, currentTime(), timeLine, isRecordChecked)
        keyTime = currentTime() + 1
        currentFeature = feature
    }

    private fun showProgress(show: Boolean) = when(show) {true -> progress.visibility = View.VISIBLE else -> progress.visibility = View.GONE }

    private fun setupCameraOptions(): SizeSelector {
        val width = SizeSelectors.maxWidth(720)
        val height = SizeSelectors.maxHeight(1280)
        val minWidth = SizeSelectors.minWidth(720)
        val minHeight = SizeSelectors.minHeight(1280)
        val dimensions = SizeSelectors.and(width, height, minWidth, minHeight)

        return SizeSelectors.or(
            dimensions
        )
    }

    private fun onCameraPermissionResult(grantResults: IntArray) = if (grantResults.isNotEmpty()
        && grantResults[0] == PackageManager.PERMISSION_GRANTED
        && grantResults[1] == PackageManager.PERMISSION_GRANTED
        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
        setCameraVisibility(View.VISIBLE)
        initCamera()
    } else {
        finish()
    }

    private fun setCameraVisibility(vis: Int) {
        camera_view.visibility = vis
    }

    private fun clearTimeLine() {
        timeLine.clear()
        finalTimeLine.clear()
    }

    private fun addToTimeLine(feature: Feature, startTime: Long, endTime: Long, timeLine: MutableList<TimeLine>, canAdd: Boolean) =
        if (canAdd)
            timeLine.add(TimeLine(feature, startTime, endTime))
        else false

    private fun initCamera() {
        camera_view.apply {
            setLifecycleOwner(this@MainActivity)
            mode = Mode.VIDEO
            addCameraListener(getCameraListener())
            setVideoSize(setupCameraOptions())
        }
        CameraLogger.registerLogger { _, _, _, throwable ->
            Log.e(TAG, throwable?.message)
        }
    }

    private fun getCameraListener() = object : CameraListener() {
        override fun onCameraError(exception: CameraException) {
            super.onCameraError(exception)
            getToast("An error has occurred, Start video recording again").show()
        }

        override fun onVideoTaken(result: VideoResult) {
            super.onVideoTaken(result)
            getToast("Video successfully captured").show()

            finalTimeLine.addAll(correctTimeLine(timeLine))
            startVideoPlayback()
        }
    }

    private fun processVideo() {
        deleteFile(finalFile)

        val openFileAndSetupBitrate = "-i ${originalVideoFile.absolutePath} -b ${4_000_000} -filter_complex "

        val complexFilterCommands = createVideoModifications(finalTimeLine)

        val videoPartsFilter = createVideoParts(finalTimeLine)

        val videoConcatFilter = "concat=n=${timeLine.size}:v=1:a=1[outv][outa]"

        val videoStreamsMap = " -map [outv] -map [outa] -preset ultrafast ${finalFile.absolutePath}"

        val command = openFileAndSetupBitrate + complexFilterCommands + videoPartsFilter + videoConcatFilter + videoStreamsMap

        execFFmpegBinary(command)
    }
    
    private fun deleteFile(file: File) = file.delete()

    private fun createVideoModifications(timeLine: List<TimeLine>) = createString(timeLine) {index, t ->
        when(t.feature) {
            Feature.NORMAL -> addToList(t, index, audioSetPts, setPts)
            Feature.SLOW_MO -> addToList(t, index,
                "${atempo(0.5f)},$audioSetPts", "${setPtsSpeed(4.0f)},$setPts")
            Feature.SPEEDUP -> addToList(t, index,
                "${atempo(2.0f)},$audioSetPts", "${setPtsSpeed(0.5f)},${setPtsSpeed(0.5f)},$setPts")
        }
    }

    private fun createVideoParts(timeLine: List<TimeLine>) = createString(timeLine) { index, t ->
        "[v$index][a$index]"
    }

    private fun createString(timeLine: List<TimeLine>, f: (Int, TimeLine) -> String): String {
        val builder = StringBuilder()

        timeLine.forEachIndexed { index, t ->
            builder.append(f.invoke(index, t))
        }
        return builder.toString()
    }

    private fun format(time: Long) = "%.3f".format(time / 1000f)

    private fun addOption(start: String, end: String, map: Int, audioOptions: String, videoOptions: String) =
        "[0:v]trim=start=$start:end=$end,$videoOptions[v$map];" +
        "[0:a]atrim=start=$start:end=$end,$audioOptions[a$map];"

    private fun correctTimeLine(timeLine: List<TimeLine>): List<TimeLine> {
        val newTimeLine = mutableListOf<TimeLine>()

        timeLine.forEachIndexed { index, it ->
            val startTime = it.startTime - startVideoTime
            val endTime = if (index == timeLine.size - 1) {
                endVideoTime - startVideoTime
            } else {
                it.endTime - startVideoTime
            }
            newTimeLine.add(TimeLine(it.feature, startTime, endTime))
        }
        return newTimeLine
    }

    private fun startVideoPlayback() {
        PlayActivity.startForResult(this, false, ACTIVITY_FOR_RESULTS_CODE)
    }

    private fun execFFmpegBinary(command: String) {
        val time = showCommandStartProcess(command)

        disposable.add(startTask(command)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(getOnTaskResultSubscriber()))

        hideCommandEndProcess(time)
    }

    private fun hideCommandEndProcess(time: Long) {
        Log.e(TAG, "\n\nTIME >>>> ${(currentTime() - time) / 1000f} <<<<\n\n")
        hideProgress()
        clearTimeLine()
    }

    private fun showCommandStartProcess(command: String): Long {
        showProgress(true)
        record_btn.isEnabled = false
        Log.e(TAG, "Starting command:\n>>>> $command\n")
        return currentTime()
    }

    private fun getOnTaskResultSubscriber(): ((Unit?, Throwable?) -> Unit) = { success, error ->
        success?.let {
            getToast("Video successfully processed").show()
            PlayActivity.start(this@MainActivity, true)
        } ?: let {
            Log.e(TAG, "onFailure\n\n\n>>> ${error?.message} \n<<<\n\n\n")
            getToast(error?.message).show()
        }
    }

    private fun startTask(command: String) = Single.create<Unit?> {
        val result = FFmpeg.execute(command)
        when (result) {
            0, 255 -> it.onSuccess(Unit)
            else -> it.onError(IllegalArgumentException("error while processing video"))
        }
    }

    private fun getToast(msg: String?) = Toast.makeText(this, msg, Toast.LENGTH_LONG)

    private fun currentTime() = System.currentTimeMillis()

    private fun hideProgress() {
        showProgress(false)
        record_btn.isEnabled = true
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName

        private const val REQUEST_CODE = 222
        private const val ACTIVITY_FOR_RESULTS_CODE = 355

        private const val audioSetPts = "asetpts=PTS-STARTPTS"
        private const val setPts = "setpts=PTS-STARTPTS"

        const val ORIGINAL_FILE_NAME = "original.mp4"
        const val FINAL_FILE_NAME = "final.mp4"

        val finalTimeLine = mutableListOf<TimeLine>()
    }
}