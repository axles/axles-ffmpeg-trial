package andorid.slowmodemo.com.view

import andorid.slowmodemo.com.R
import andorid.slowmodemo.com.extentions.createStorageDirFile
import andorid.slowmodemo.com.model.Feature
import andorid.slowmodemo.com.model.TimeLine
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_play.*
import java.io.File
import java.util.concurrent.TimeUnit

class PlayActivity : AppCompatActivity() {
    private var player:SimpleExoPlayer? = null
    private var showMenu: Boolean = false
    private val disposable = CompositeDisposable()

    private lateinit var original: File
    private lateinit var final: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play)

        setupActionBar()
        createFiles()

        showMenu = intent.getBooleanExtra(SHOW_MENU, false)
        setupProcessVisibility(showMenu)

        process.setOnClickListener {
            acceptAndFinish()
        }
    }

    override fun onStart() {
        super.onStart()
        initializePlayer(this)
    }

    override fun onResume() {
        super.onResume()
        initializePlayer(this)
    }

    override fun onPause() {
        releasePlayer()
        super.onPause()
    }

    override fun onStop() {
        releasePlayer()
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (showMenu) {
            menuInflater.inflate(R.menu.menu_playback, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?) = when (item?.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.processed -> {
            playVideo(final)
            true
        }
        R.id.original -> {
            playVideo(original)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        declineAndFinish()
    }

    private fun setupProcessVisibility(showMenu: Boolean) {
        process.visibility = if (showMenu) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

    private fun createFiles() {
        original = createStorageDirFile(this, MainActivity.ORIGINAL_FILE_NAME)
        final = createStorageDirFile(this, MainActivity.FINAL_FILE_NAME)
    }

    private fun setupActionBar() = supportActionBar?.let {
        it.show()
        it.setDisplayHomeAsUpEnabled(true)
        it.setHomeButtonEnabled(true)
    }

    private fun acceptAndFinish() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun declineAndFinish() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    private fun initializePlayer(context: Context) {
        createPlayerIfNull(context)
        val file = getVideoFileForPlayer()
        playVideo(file)
    }

    private fun getVideoFileForPlayer() = if (!showMenu) {
        original
    } else {
        final
    }

    private fun createPlayerIfNull(context: Context) {
        if (player == null) {
            player = ExoPlayerFactory.newSimpleInstance(
                DefaultRenderersFactory(context),
                DefaultTrackSelector(),
                DefaultLoadControl())

            camera_view.player = player
        }
    }

    private fun playVideo(file: File) {
        val mediaSource = buildMediaSource(Uri.fromFile(file))
        player?.prepare(mediaSource, false, false)
        player?.addListener(getExoPlayerListener())
    }

    private fun getExoPlayerListener() = object : Player.EventListener {
        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {}

        override fun onSeekProcessed() {}

        override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {}

        override fun onPlayerError(error: ExoPlaybackException?) {}

        override fun onLoadingChanged(isLoading: Boolean) {}

        override fun onPositionDiscontinuity(reason: Int) {}

        override fun onRepeatModeChanged(repeatMode: Int) {}

        override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {}

        override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) { }

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
           processStateChange(playWhenReady)
        }

    }

    private fun processStateChange(playWhenReady: Boolean) {
        if (showMenu) {
            return
        }

        if (playWhenReady) {
            dispose()
            disposable.add(Observable
                .interval(5L, TimeUnit.MILLISECONDS)
                .timeInterval()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    player?.let { pl ->
                        val timeLinePart = getTimeLinePart { pl.currentPosition in it.startTime..it.endTime }
                        when (timeLinePart?.feature) {
                            Feature.NORMAL -> pl.playbackParameters = PlaybackParameters(1f, 1f)
                            Feature.SLOW_MO -> pl.playbackParameters = PlaybackParameters(0.25f, 0.25f)
                            Feature.SPEEDUP -> pl.playbackParameters = PlaybackParameters(4f, 4f)
                        }
                    }
                })
        } else {
            dispose()
        }
    }

    private fun getTimeLinePart(filter: (timeLine: TimeLine) -> Boolean) = MainActivity.finalTimeLine.firstOrNull(filter)

    private fun buildMediaSource(uri: Uri): MediaSource {
        val userAgent = Util.getUserAgent(this, getString(R.string.app_name))

        return ExtractorMediaSource
            .Factory(DefaultDataSourceFactory(this, userAgent))
            .setExtractorsFactory(DefaultExtractorsFactory())
            .createMediaSource(uri)
    }

    private fun releasePlayer() {
        player?.let {
            it.release()
            player = null
        }
        dispose()
    }

    private fun dispose() {
        player?.playbackParameters = playbackParams(1f, 1f)
        disposable.clear()
    }

    private fun playbackParams(speed: Float, pitch: Float) = PlaybackParameters(speed, pitch)

    companion object {
        private const val SHOW_MENU = "SHOW_MENU"

        fun start(context: Context, showMenu: Boolean) {
            val intent = getIntent(context, showMenu)
            context.startActivity(intent)
        }

        fun startForResult(
            activity: AppCompatActivity,
            showMenu: Boolean,
            code: Int) {
            val intent = getIntent(activity, showMenu)
            activity.startActivityForResult(intent, code)
        }

        fun getIntent(context: Context, showMenu: Boolean)
                = Intent(context, PlayActivity::class.java).also {
            it.putExtra(SHOW_MENU, showMenu)
        }
    }
}